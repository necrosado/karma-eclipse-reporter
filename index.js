var EclipseReporter = function(baseReporterDecorator, logger, formatError) {
	baseReporterDecorator(this);

	var that = this;
	var net = require("net");
	
	var _log = logger.create("reporter.eclipse");
	var errors = [];
	
	this.onBrowserStart = function(browser){
		errors = [];
	};
	
	this.onRunComplete = function(browsers, results) {
		
		var socket = net.Socket();
		
		socket.on("error", function(data) {
			_log.warn("Cannot connect to Eclipse");
		});
		
		socket.on('connect', function(data){
			socket.write('version=KARMA-1.0.0');
			socket.write(JSON.stringify(errors));
			
			socket.end();
		});
		
		socket.connect(37357, '127.0.0.1');
	};

	this.onSpecFailure = function(browsers, results) {
 		errors.push(results);
	};
	
	this.specFailure = this.onSpecFailure;
	this.specSuccess = this.onSpecFailure;
};

EclipseReporter.$inject = [ 'baseReporterDecorator', 'logger', 'formatError'];

module.exports = {
	'reporter:eclipse' : [ 'type', EclipseReporter ]
};
