var io = require('socket.io');
var http = require('http');

var app = http.createServer(function(req, res){
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.end("test");
});

// Socket.io server listens to our app
var socket = io.listen(app);

socket.on('connection', function(socket) {
	console.log("Connected");
});

app.listen(process.argv[2], function(){});
		
